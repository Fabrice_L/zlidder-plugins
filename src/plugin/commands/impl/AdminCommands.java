package plugin.commands.impl;

import com.zlidder.model.Location;
import com.zlidder.model.item.GameItem;
import com.zlidder.model.npc.Npc;
import com.zlidder.model.player.Player;
import com.zlidder.model.pulse.Pulse;
import com.zlidder.model.pulse.PulseOwner;
import com.zlidder.model.pulse.PulseType;
import com.zlidder.model.pulse.impl.TestPulse;
import com.zlidder.net.packet.outgoing.CreateObject;
import com.zlidder.net.packet.outgoing.SendMessage;
import com.zlidder.world.NpcHandler;
import com.zlidder.world.World;

public class AdminCommands {
	
	private Player player;
	private String[] command;

	public AdminCommands(Player player, String[] command) {
		this.player = player;
		this.command = command;
	}
	
	public void doCommand() {
		if (command[0].equals("object")) {
			player.send(new CreateObject(1278, 11, player.getLocation().getX(), player.getLocation().getY(), 3));
		}
		if (command[0].equals("npcs")) {
			for (Npc npc : NpcHandler.npcs) {
				if (npc == null) {
					continue;
				}
				if (npc.getNpcDefinition().getId() == 1) {
					Pulse pulse = new TestPulse();
					pulse.setPulseOwner(new PulseOwner(1, PulseType.NPC));
					World.submit(pulse);
				}
			}
		}
		if (command[0].equals("hit")) {
			int npcId = Integer.parseInt(command[1]);
			int hitDiff = Integer.parseInt(command[2]);
			for (Npc npc : NpcHandler.npcs) {
				if (npc == null) {
					continue;
				}
				if (npc.getNpcDefinition().getId() == npcId) {
					System.out.println("npc found: " + npc.getNpcDefinition().getName());
					npc.handleHitMask(hitDiff);
				}
			}
		}
		if (command[0].equalsIgnoreCase("item")) {
			try {
				int itemId = Integer.parseInt(command[1]);
				int itemAmount = Integer.parseInt(command[2]);
				player.getInventoryContainer().addItem(new GameItem(itemId, itemAmount));
			} catch (Exception exception) {
				player.send(new SendMessage("Used as ::item ITEMID AMOUNT"));
			}
		}
		
		if (command[0].equalsIgnoreCase("coords")) {
			player.send(new SendMessage("Location: " + player.getLocation().getX() + ", " + player.getLocation().getY() + ", " + player.getLocation().getHeight()));
		}
		
		if (command[0].equalsIgnoreCase("tele")) {
			try {
				int x = Integer.parseInt(command[1]);
				int y = Integer.parseInt(command[2]);
				int z = command.length == 4 ? Integer.parseInt(command[3]) : 0;
				
				player.setNextLocation(new Location(x, y, z));
			} catch (Exception exception) {
				player.send(new SendMessage("Used as ::tele X Y Z"));
			}
		}
		
	}

}
