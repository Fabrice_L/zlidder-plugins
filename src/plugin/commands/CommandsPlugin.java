package plugin.commands;

import com.zlidder.model.player.Player;
import com.zlidder.plugin.impl.IncomingPacketPlugin;

import plugin.commands.impl.AdminCommands;
import plugin.commands.impl.ModCommands;
import plugin.commands.impl.PlayerCommands;

public class CommandsPlugin extends IncomingPacketPlugin {
	
	public static void main(String[] args) {
		/**
		 * For Setting the main class
		 */
	}

	@Override
	public int getOpcode() {
		return 103;
	}

	@Override
	public void send(Player player) {
		String command = player.packetManager().getInStream().readString();
		String[] args = command.split(" ");	
		if (player.playerRights >= 2) {
			new AdminCommands(player, args).doCommand();
		}
		if (player.playerRights >= 1) {
			new ModCommands(player, args).doCommand();
		}
		new PlayerCommands(player, args).doCommand();
	}
}
