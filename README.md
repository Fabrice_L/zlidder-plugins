# ZLIDDER PLUGINS #

In this repository, you'll be able to find all the plugins I write for the zlidder framework.

Once the plugin system serversided is ready, I'll also be accepting pull request from anyone willing to contribute to the continued development of this framework.

## How To Use Plugins ##
To use the plugins, you have to jar them, when jarring them, make sure to set the main-class, else the plugin won't work as it's supposed to (it won't even work at all).

If you have any questions, feel free to contact me on skype (mod.page)

## Contributors ##
* Fabrice L